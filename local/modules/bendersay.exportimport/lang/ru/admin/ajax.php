<?php
$MESS['BENDERSAY_EXPORTIMPORT_ERROR'] = '������ AJAX �������';
$MESS['BENDERSAY_EXPORTIMPORT_RESULT_TEXT'] = '���� ���������';
$MESS['BENDERSAY_EXPORTIMPORT_PROGRESS_BAR'] = '������� ������';
$MESS['BENDERSAY_EXPORTIMPORT_PROGRESS_BAR_IMPORT'] = '������ ������';
$MESS['BENDERSAY_EXPORTIMPORT_PROGRESS_BAR_IMPORT_IZ'] = '��';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_EMPTY'] = '��� ���� �����������';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE_SAVE_ERROR'] = '������� Highload-����� ���������� �������';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE_SAVE'] = '������� Highload-����� � CSV ������� ��������, CSV ����� ������� �� ������: ';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE_SAVE_JSON'] = '������� Highload-����� � JSON ������� ��������, JSON ����� ������� �� ������: ';
$MESS['BENDERSAY_EXPORTIMPORT_FINISH_IMPORT'] = '������ Highload-����� �� JSON ��������';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE_SAVE_SEND'] = '������� �� email: ';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE_SAVE_SEND_BUTTON'] = '���������';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_GETUSERENTITYIMPORT'] = '���� ��� ������� �� ��� ������ ��� �� ���������� �� �������';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE_SIZE'] = '���� ��� ������� ����� ������� #file_size#M. ������ ��������� ����������� ������ #memory_limit# �� �������� ������������� JSON';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE_SIZE_EXP'] = '���� �������� ������ #file_size#M. ������ ��������� ����������� ������ #memory_limit# �� �������� �������������� JSON.'
	.' <br>�������� <i>memory_limit</i> � php.ini ������� ';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_NOT_KEY'] = '� ������������� ����� ����������� ������������ ���� � ������ #key#';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_COUNT'] = '�������� "items_all_count" �� ��������� � �������� ����������� ������� "items"';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_IMPORT_FILE'] = '��� ������� ����� �������� ������, ����������: #count#.  ������� ��� ����� �� ������: ';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_IMPORT_FILE_FIELD'] = '������ #key# ���������������, �� ����(�) c ID "#prop#" �� ��������(�)';
$MESS['BENDERSAY_EXPORTIMPORT_GETUSERENTITYIMPORT_ZAG'] = '<tr><th>���� Highload-�����</th><th>���� �������������� �����</th></tr>';
$MESS['BENDERSAY_EXPORTIMPORT_ZAGLUSHKA'] = '<span class="required">��������!</span> ���������� ������� ��������� � ����������.'
	. ' ��������� � ������� ��� ��������� ������� <a href="mailto:anton-capi@mail.ru?subject=������ �� ������ �������/������ Highload-������ � CSV, JSON">anton-capi@mail.ru</a>';