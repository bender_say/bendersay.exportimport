<?php
$MESS['BENDERSAY_EXPORTIMPORT_TITLE'] = '������ ������ Highload-����� �� CSV';
$MESS['BENDERSAY_EXPORTIMPORT_TITLE_IMPORT'] = '������ ������ Highload-�����';
$MESS['BENDERSAY_EXPORTIMPORT_FIELD_IMPORT_FILE'] = '���� ��� �������';
$MESS['BENDERSAY_EXPORTIMPORT_EXPORT_DELIMITER'] = '����������� �����';
$MESS['BENDERSAY_EXPORTIMPORT_EXPORT_ENCLOSURE'] = '������������ �����';
$MESS['BENDERSAY_EXPORTIMPORT_EXPORT_DELIMITER_M'] = '����������� ������������� �����';
$MESS['BENDERSAY_EXPORTIMPORT_EXPORT_CODING'] = '��������� ����� CSV';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_O_SET'] = '�������� ���������';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_HL_DATA'] = '��������� ������� ������';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_HL_STRUCTURE'] = '��������� ������� ��������� Highload-�����';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_HL'] = 'Highload-����';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_HL_NEW'] = '--������� ����� Highload-����--';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_HLS'] = '������������� ���������';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_DATA'] = '������������� ������';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_CLUCH'] = '���� �������� �����';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_COMPARING'] = '������������ ����� �� Highload-����� ����� �� �����';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_EXPORT_COUNT_ROW'] = '���������� ����� ������������� �� ����� ����';
$MESS['BENDERSAY_EXPORTIMPORT_IMPORT_USERENTITY'] = '��������� ���� Highload-�����<br>(������ ��� �������� ������)';
$MESS['BENDERSAY_EXPORTIMPORT_START_EXPORT'] = '�������������';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_HIGHLOADBLOCK'] = '��� ������ ������� ������� ���������� ������� ������ "highloadblock"';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_MODULE'] = '������ �������/������ Highload-������ � CSV, JSON �� ���������';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_NOT_ID'] = '�� ������ Highload-���� � ID=#ID#';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_FILE'] = '������ ������ �����';
$MESS['BENDERSAY_EXPORTIMPORT_ERROR_GETUSERENTITYIMPORT'] = '���� ��� ������� �� ��� ������ ��� �� ���������� �� �������';
$MESS['BENDERSAY_EXPORTIMPORT_INFO'] = '������ ������ CSV �����, ������ <b>�����������</b> ��������� �������� ������������� �����.';
$MESS['BENDERSAY_EXPORTIMPORT_INFO_2'] = '� ��������� ����������� ���� �� �������������� �����,'
	. ' �� ��������� �������� ����� ����������� ����� �������, �� ���� <b>ID</b> � highload-�����. ���� ������ �������, '
	. '�� �� ���� ����� ��������� � ������������ �� ���������� � �����. ���� ������ �����������, '
	. '�� ��� ����� ��������� � highload-����. ���� � ��������� ���� �� �������, '
	. '�� ��� ������ �� ����� ����� ��������� � highload-���� ��� ����� (���� ���� ��� ��������� �����������).';